<?php

namespace Keks_Pay\KeksPay\Gateway\Command;

use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Phrase;
use Magento\Payment\Gateway\Command;
use Magento\Payment\Gateway\Command\CommandException;
use Magento\Payment\Gateway\CommandInterface;
use Magento\Payment\Model\InfoInterface;
use Magento\Sales\Api\Data\OrderInterface;
use Keks_Pay\KeksPay\Api\Data\Payment\RefundRequestInterfaceFactory;
use Keks_Pay\KeksPay\Api\Data\PaymentStatusCode;
use Keks_Pay\KeksPay\Api\KeksPaymentStatusRepositoryInterface;
use Keks_Pay\KeksPay\Api\Payment\PaymentInterface;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class Refund implements CommandInterface
{

    // The time that the transactions should be in eur
    const EUR_CONVERSION_RATE = 7.53450;

    /**
     * @var KeksPaymentStatusRepositoryInterface
     */
    private $keksPaymentStatusRepository;

    /**
     * @var PaymentInterface
     */
    private $payment;

    /**
     * @var RefundRequestInterfaceFactory
     */
    private $refundRequestFactory;

    public function __construct(
        RefundRequestInterfaceFactory $refundRequestFactory,
        PaymentInterface $payment,
        KeksPaymentStatusRepositoryInterface $keksPaymentStatusRepository
    ) {
        $this->keksPaymentStatusRepository = $keksPaymentStatusRepository;
        $this->payment = $payment;
        $this->refundRequestFactory = $refundRequestFactory;
    }

    /**
     * @param array $commandSubject
     * @return Command\ResultInterface|void|null
     * @throws CommandException
     * @throws AlreadyExistsException
     */
    public function execute(array $commandSubject)
    {
        /**
         * @var InfoInterface $payment
         */
        $payment = $commandSubject['payment']->getPayment();
        $amount = $commandSubject['amount'];
        /**
         * @var OrderInterface $order
         */
        $order = $payment->getOrder();

        $status = $this->keksPaymentStatusRepository->getById((int) $order->getIncrementId());

        if ($status === null) {
            throw new CommandException(
                new Phrase('Payment status not found.')
            );
        }

        // check if its paid
        if ($status->getStatus() !== PaymentStatusCode::OK
            && $status->getMessage() !== PaymentStatusCode::PAID_MESSAGE) {
            throw new CommandException(
                new Phrase('Expected payment status: ' . PaymentStatusCode::OK
                    . ', got ' . $status->getStatus())
            );
        }

        $refundRequest = $this->refundRequestFactory->create();

        // check if the original transaction was done in HRK or EUR, if HRK - recalculate
        $amount = $this->normalizeToEur($amount, $order);

        $refundRequest
            ->setTid($status->getTid())
            ->setKeksId($status->getKeksId())
            ->setBillId($status->getBillId())
            ->setAmount((string) $amount)
            ->createRequest();

        $refundResult = $this->payment->refund($refundRequest);

        if (!in_array($refundResult->getHttpStatus(), [200, 204])) {
            throw new CommandException(
                new Phrase('Payment refund failed. HTTP status: %1', [$refundResult->getHttpStatus()])
            );
        }

        $receivedData = $refundResult->getPayload();

        $rcvStatus = null;

        if ($receivedData->hasData('status')) {
            $rcvStatus = (int) $receivedData->getData('status');
        }

        $rcvMessage = (string) $receivedData->getData('message');

        if ($rcvStatus === null || $rcvStatus !== PaymentStatusCode::OK) {
            throw new CommandException(
                new Phrase('Payment refund failed. Message: %1', [$rcvMessage])
            );
        }

        $status->setMessage($rcvMessage);
        $status->setStatus($rcvStatus);
        $this->keksPaymentStatusRepository->save($status);

        $payment->setTransactionId($receivedData->getData('keks_refund_id'))->setIsTransactionClosed(1);

        return null;
    }


    /**
     * Created because of API issues regarding refunds of transactions made in HRK but refunded in EUR
     * @param $amount
     * @param $order
     * @return float
     */
    public function normalizeToEur($amount, $order): float
    {
        // Check if the order was created in HRK
        if($order->getOrderCurrencyCode() !== "HRK") {
            return $amount;
        }

        // Convert the amount according to the fixed conversion rate
        return round($amount / self::EUR_CONVERSION_RATE, 2);
    }
}
