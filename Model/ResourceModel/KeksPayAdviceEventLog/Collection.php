<?php

namespace Keks_Pay\KeksPay\Model\ResourceModel\KeksPayAdviceEventLog;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * @SuppressWarnings("camelCase")
 */
class Collection extends AbstractCollection
{
    // phpcs:ignore
    protected $_idFieldName = 'entity_id';

    // phpcs:ignore
    protected function _construct()
    {
        $this->_init(
            \Keks_Pay\KeksPay\Model\KeksPayAdviceEventLog::class,
            \Keks_Pay\KeksPay\Model\ResourceModel\KeksPayAdviceEventLog::class
        );
    }
}
