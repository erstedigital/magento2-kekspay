<?php

namespace Keks_Pay\KeksPay\Model\ResourceModel\KeksPaymentStatus;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * @SuppressWarnings("camelCase")
 */
class Collection extends AbstractCollection
{
    // phpcs:ignore
    protected $_idFieldName = 'order_id';

    // phpcs:ignore
    protected function _construct()
    {
        $this->_init(
            \Keks_Pay\KeksPay\Model\KeksPaymentStatus::class,
            \Keks_Pay\KeksPay\Model\ResourceModel\KeksPaymentStatus::class
        );
    }
}
