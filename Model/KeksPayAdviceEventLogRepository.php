<?php

namespace Keks_Pay\KeksPay\Model;

use Keks_Pay\KeksPay\Api\Data\KeksPayAdviceEventLogInterface;
use Keks_Pay\KeksPay\Api\Data\KeksPayAdviceEventLogInterfaceFactory;
use Keks_Pay\KeksPay\Api\KeksPayAdviceEventLogRepositoryInterface;
use Keks_Pay\KeksPay\Model\ResourceModel\KeksPayAdviceEventLog as KeksPayAdviceEventLogResourceModel;
use Keks_Pay\KeksPay\Model\ResourceModel\KeksPayAdviceEventLog\CollectionFactory;
use Keks_Pay\KeksPay\Model\ResourceModel\KeksPayAdviceEventLogFactory as KeksPayAdviceEventLogResourceModelFactory;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class KeksPayAdviceEventLogRepository implements KeksPayAdviceEventLogRepositoryInterface
{
    /**
     * @var KeksPayAdviceEventLogResourceModelFactory
     */
    private $resourceModelFactory;

    /**
     * @var KeksPayAdviceEventLogInterfaceFactory
     */
    private $modelFactory;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    public function __construct(
        KeksPayAdviceEventLogResourceModelFactory $resourceModelFactory,
        KeksPayAdviceEventLogInterfaceFactory $modelFactory,
        CollectionFactory $collectionFactory
    ) {
        $this->resourceModelFactory = $resourceModelFactory;
        $this->modelFactory = $modelFactory;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * @inheritDoc
     */
    public function save(KeksPayAdviceEventLogInterface $keksPayAdviceEventLog): void
    {
        $this->getResourceModel()->save($keksPayAdviceEventLog);
    }

    /**
     * @inheritDoc
     */
    public function getById(int $keksPayAdviceEventLogId): ?KeksPayAdviceEventLogInterface
    {
        $model = $this->getModel();

        $this->getResourceModel()->load($model, $keksPayAdviceEventLogId);

        return ($model->getId()) ? $model : null;
    }

    /**
     * @inheritDoc
     */
    public function delete(KeksPayAdviceEventLogInterface $keksPayAdviceEventLog): void
    {
        $this->getResourceModel()->delete($keksPayAdviceEventLog);
    }

    /**
     * @inheritDoc
     */
    public function deleteById(int $keksPayAdviceEventLogId)
    {
        $model = $this->getById($keksPayAdviceEventLogId);

        if ($model) {
            $this->getResourceModel()->delete($model);
        }
    }

    /**
     * @inheritDoc
     */
    public function getModel(): KeksPayAdviceEventLogInterface
    {
        return $this->modelFactory->create();
    }

    /**
     * @return KeksPayAdviceEventLogResourceModel
     */
    private function getResourceModel(): KeksPayAdviceEventLogResourceModel
    {
        return $this->resourceModelFactory->create();
    }
}
