<?php

namespace Keks_Pay\KeksPay\Model\Client\Exception;

use Magento\Framework\Exception\LocalizedException;

class ClientException extends LocalizedException
{

}
