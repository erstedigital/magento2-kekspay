<?php

namespace Keks_Pay\KeksPay\Model\Client;

use Keks_Pay\KeksPay\Model\Client\Request\RequestInterface;
use Keks_Pay\KeksPay\Model\Client\Response\ResponseInterface;

interface ClientInterface
{
    /**
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function doRequest(RequestInterface $request): ResponseInterface;
}
