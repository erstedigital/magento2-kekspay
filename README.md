# KeksPay extension installation instructions

## Extension constraints
 - Requires Magento v.2.3 or above
 - Requires KeksPay Account

### Installation
Choose one of the installation methods described below.

#### Install with Composer

This is recommended installation method, make sure you are familiar with Magento Composer workflow and everything is set accordingly on the project: https://devdocs.magento.com/extensions/install/
 - At Magento Marketplace, add extension to the cart and go through the checkout process to be able to download with Composer (extension is free)
 - Run following commands from command line:
   - composer require keks_pay/kekspay
   - bin/magento setup:upgrade
   - Follow additional instructions if Magento displays them
   
#### Install manually
 - Contact KeksPay to send you packed extension
 - Create app/code/Keks_Pay/KeksPay/ directory
 - Unzip content of the file to newly created KeksPay
 - Using the command line, run this command:
   - bin/magento setup:upgrade
   - Follow additional instructions if Magento displays them.

### Configuration
To enable and configure KeksPay go to the Magento Admin Panel. Prepare credentials you got from KeksPay.
- Navigate to Stores > Configuration > Sales > Payment Methods > Other Payment Methods > “Keks_Pay KEKS Pay”.
- Under Enabled, select Yes.
- Fill in all of the required configuration fields that you’ve got from KeksPay
- Save your configuration and follow Magento’s prompt to clear the Cache.
