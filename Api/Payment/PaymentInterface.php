<?php

namespace Keks_Pay\KeksPay\Api\Payment;

use Keks_Pay\KeksPay\Api\Data\Payment\RefundRequestInterface;
use Keks_Pay\KeksPay\Api\Data\Transaction\AllTransactionInfoRequestInterface;
use Keks_Pay\KeksPay\Api\Data\Transaction\TransactionInfoRequestInterface;
use Keks_Pay\KeksPay\Model\Client\Response\ResponseInterface;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
interface PaymentInterface
{
    /**
     * @param RefundRequestInterface $refundRequest
     * @return ResponseInterface
     */
    public function refund(RefundRequestInterface $refundRequest): ResponseInterface;

    /**
     * @param TransactionInfoRequestInterface $transactionInfoRequest
     * @return ResponseInterface
     */
    public function transactionInfo(TransactionInfoRequestInterface $transactionInfoRequest): ResponseInterface;

    /**
     * @param AllTransactionInfoRequestInterface $transactionInfoRequest
     * @return ResponseInterface
     */
    public function allTransactionInfo(AllTransactionInfoRequestInterface $transactionInfoRequest): ResponseInterface;
}
